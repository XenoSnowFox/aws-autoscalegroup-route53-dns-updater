# autoscalegroup_route53_dns_updated.py

Lambda function to add/remove ec2 instances from Route53 in reaction to LAUNCH or TERMINATE events.

1. Set up Lambda function
2. Create a SNS topic
3. Subscribe the Lambda function to the SNS topic
4. Add lifecycle hooks to your auto-scaling groups to send events to SNS